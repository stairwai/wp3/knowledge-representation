# Knowledge Representation for StairwAI AMS API

## Overview

The "knowledge-representation" repository is dedicated to establishing a foundation for the use and ongoing updates of the StairwAI AMS API. It is designed to integrate and align with the StairwAI ontology, ensuring that the API functions smoothly with the defined constraints and guidelines. This repository is structured into four main components:

### 1. `swai_ams.json` File

The `swai_ams.json` file contains a set of constraints and guidelines necessary for the functioning of the StairwAI AMS API. These constraints are generated based on the StairwAI ontology ([Ontology Repository Link](https://gitlab.com/stairwai/wp3/ontology)). Changes to the ontology can impact these constraints, necessitating updates to this file.

### 2. `JSON_generator` Directory

The `JSON_generator` directory houses tools for automatically updating the `swai_ams.json` file. These updates reflect any changes made to the ontology, ensuring consistency and accuracy. To use the JSON generator:

   - Navigate to the folder:
     ```bash
     cd JSON_generator
     ```
   - Run the bash script (as administrator) to install requirements and update the JSON file:
     ```bash
     sudo ./auto_json_generator.sh
     ```

### 3. `doc-api` Directory

The `doc-api` directory serve as an example of how to structure new functionalities and the documents the basic AMS-API functionalities. This documentation is crucial for maintaining consistency and clarity, especially if additional functionalities need to be integrated in the future.

### 4. `empty_DB_instance` Directory

This directory generates a Docker container with an empty Neo4j instance, with the same library version as the AMS. Useful to dynamically run the changes made on the AMS-API. To generate this instance: 

   - Navigate to the folder:
     ```bash
     cd empty_DB_instance
     ```
   - Run the Docker container as admin:
     ```bash
     sudo docker-compose up
     ```

## Getting Started

To get started with this repository:

1. Clone the repository to your local machine.
2. Follow the instructions in each component's section to understand and interact with the different parts of the repository.

## Contributing

Contributions to this repository are welcome. Please ensure that any changes are consistent with the existing structure and are thoroughly tested.

## License

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 2.0 Generic (CC BY-NC-SA 2.0) License.

## Contact

For any queries or contributions, please contact miquel.buxons@upc.edu and javier.vazquez-salceda@upc.edu.

