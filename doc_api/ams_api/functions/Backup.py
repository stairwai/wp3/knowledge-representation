from .Creators import *
import os
import pickle


def hard_db_reset(driver: NeoDriver, with_backup: bool = True, path: str = ''):
    """
    Hard reset function, this functionality allows the possibility to reset the AMS, e.g. in the case of StairwAI
    ontology update. This function automatically stores all the inserted instances in a backup file, restarts the
    AMS, imports the latest StairwAI ontology available, makes the changes to the swai_ams.json file and finally
    incorporates the instances available in the backup. WARNING: currently, an AMS reset will change all the node
    identifiers!
    :param driver: Driver object which contains the connection to the database and the static variables
    :param with_backup: Boolean parameter, if it is set to true a backup of the AMS instances is done before the
    reset, after it the instances are loaded to the AMS again. If it is set to False, the AMS INSTANCES WILL BE
    COMPLETELY AND PERMANENTLY DELETED.
    :param path: string indicating the path to store the backup files.
    """
    if type(with_backup) is not bool:
        raise ValueError("with_backup parameter has to be a boolean.")
    if type(path) is not str:
        raise ValueError("path parameter has to be a String.")

    # create the backup files
    if with_backup:
        create_backup(driver, path=path)
    # delete all the nodes and relations of the database
    driver.graph.run(
        "CALL apoc.periodic.iterate('MATCH (n) RETURN n', 'DETACH DELETE n', {batchSize:1000, iterateList:true})")
    driver.graph.run("CALL n10s.nsprefixes.removeAll();")
    driver.graph.run("CALL apoc.schema.assert({}, {});")
    # initialise again the database, loading the ontologies
    init_db(driver=driver)
    # Import the latest static variables JSON file version
    driver = NeoDriver(is_reset=True)
    # create the necessary instanceOf relations
    create_instance_of(driver=driver)
    # insert new internal database identifiers to all the instances already created (usually the ones specified in the ontologies)
    for individual in driver.graph.nodes.match('owl__NamedIndividual').all():
        individual['swai__identifier'] = individual.identity
        individual['swai__uuidIdentifier'] = str(uuid.uuid1())
        driver.graph.push(individual)
    print("Reset successfully done")
    # read the backup files and load to the db
    if with_backup:
        read_backup(driver=driver, path=path)
    return driver


def init_db(driver: NeoDriver):
    """
    This function initialize a StairwAI AMS new instance running the importing ontology commands and setting the
    prefixes required.
    :param driver: Driver object which contains the connection to the database and the static variables
    """
    # initialise the database using the commands to set the prefixes and loading the ontologies
    print("Re-initialising the Neo4j database...")
    driver.graph.run("CALL n10s.graphconfig.init();")
    driver.graph.run("CREATE CONSTRAINT n10s_unique_uri ON (r:Resource) ASSERT r.uri IS UNIQUE;")
    driver.graph.run("CALL n10s.nsprefixes.add('cry', 'https://stairwai.gitlab.io/wp3/ontology/Country.rdf#');")
    driver.graph.run("CALL n10s.nsprefixes.add('aicat', 'https://stairwai.gitlab.io/wp3/ontology/topics.rdf#');")
    driver.graph.run("CALL n10s.nsprefixes.add('aitec', 'https://stairwai.gitlab.io/wp3/ontology/ai_onto.rdf#');")
    driver.graph.run("CALL n10s.nsprefixes.add('swai', 'http://purl.org/swai/spec#');")
    driver.graph.run("CALL n10s.nsprefixes.add('owg', 'http://purl.org/owg/onto/spec#');")
    driver.graph.run("CALL n10s.nsprefixes.add('foaf', 'http://xmlns.com/foaf/0.1/');")
    driver.graph.run("CALL n10s.nsprefixes.add('dc', 'http://purl.org/dc/elements/1.1/');")
    driver.graph.run("CALL n10s.nsprefixes.add('dcterms', 'http://purl.org/dc/terms/');")
    driver.graph.run("CALL n10s.rdf.import.fetch('http://purl.org/swai/spec','RDF/XML');")
    driver.graph.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/topics.rdf', 'RDF/XML');")
    driver.graph.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/Country.rdf', 'RDF/XML');")
    driver.graph.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/ai_onto.rdf', 'RDF/XML');")


def create_backup(driver: NeoDriver, path: str = ""):
    """
    This method creates a StairwAI AMS back-up, with the information extracted from the nodes and relations it
    creates two pickle files where these data are stored. Use 'read_backup()' function in order to store again the
    files information to the database.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param path: string indicating the path to store the back-up files.
    """
    if type(path) is not str:
        raise ValueError("path parameter has to be a String.")

    print("Creating back-up files ...")
    # extract instantiable class ontology names
    insta_labels = get_instantiable_classes(driver=driver)
    insta_onto = [driver.class_owl.loc[driver.class_owl['label'] == label].db_name.values[0] for label in insta_labels]
    # get the non instantiable class label
    nn_insta = [elem for elem in get_classes(driver) if elem not in insta_labels]
    non_insta_list = []
    for label in nn_insta:
        db_name = driver.class_owl.loc[driver.class_owl['label'] == label].db_name.values[0]
        for prx, uri in driver.prefixes.items():
            for node2 in driver.graph.nodes.match(prx + "__" + db_name).all():
                non_insta_list.append(
                    {'name': node2['rdfs__label'], 'class': label, 'old_id': node2['swai__identifier']})
    nodes = []
    relations = []
    inv_buffer = {}
    att_list = [elem for elem in all_database_attributes(driver=driver, with_conditions=False)]
    for db_name in insta_onto:
        for node2 in driver.graph.nodes.match("swai__" + db_name).all():
            # node information
            node_dict = {}
            for key, value in node_data_to_dict(driver=driver, node=node2).items():
                if key in att_list or key == 'class':
                    # Data properties
                    node_dict[key] = value
            nodes.append(node_dict)
            # relations
            for r in driver.graph.match(nodes=(node2,)).all():
                if str(r.__class__.__name__) == 'owl__instanceOf':
                    continue
                df_row = driver.properties.loc[driver.properties["db_name"] == str(r.__class__.__name__).split("__")[-1]]
                if str(r.__class__.__name__) in ['swai__score', 'swai__scoredIn']:
                    # if the inverse is already stored, do not store
                    if str(r.start_node['swai__identifier']) + '-' + str(
                            r.end_node['swai__identifier']) in inv_buffer:
                        continue
                    # store the relation
                    relations.append({'relation': str(df_row["label"].values[0]),
                                      'start_node': r.start_node['swai__identifier'],
                                      'end_node': r.end_node['swai__identifier'],
                                      'score': r['score']})
                    # if inverse exist prepare information in the buffer to not store the inverse (avoid memory overload)
                    if driver.inverses[df_row["db_name"].values[0]] is not None:
                        inv_buffer[str(r.end_node['swai__identifier']) + '-' + str(
                            r.start_node['swai__identifier'])] = 'done'
                else:
                    # if the inverse is already stored, do not store
                    if str(r.start_node['swai__identifier']) + '-' + str(
                            r.end_node['swai__identifier']) in inv_buffer:
                        continue
                    # store the relation
                    relations.append({'relation': str(df_row["label"].values[0]),
                                      'start_node': r.start_node['swai__identifier'],
                                      'end_node': r.end_node['swai__identifier']})
                    # if inverse exist prepare information in the buffer to not store the inverse (avoid memory overload)
                    if driver.inverses[df_row["db_name"].values[0]] is not None:
                        inv_buffer[str(r.end_node['swai__identifier']) + '-' + str(
                            r.start_node['swai__identifier'])] = 'done'
    # store the nodes extracted from the database
    with open(os.path.join(path, 'nodes_backup.pickle'), 'wb') as handle:
        pickle.dump(nodes, handle, protocol=pickle.HIGHEST_PROTOCOL)
    # store the relations extracted from the database
    with open(os.path.join(path, 'relations_backup.pickle'), 'wb') as handle:
        pickle.dump(relations, handle, protocol=pickle.HIGHEST_PROTOCOL)
        # store the relations extracted from the database
    with open(os.path.join(path, 'non_instantiable_backup.pickle'), 'wb') as handle:
        pickle.dump(non_insta_list, handle, protocol=pickle.HIGHEST_PROTOCOL)
    print("Back-up files created.")


def read_backup(driver: NeoDriver, path: str = ""):
    """
    This method read the pickle files with the AMS instances back-up generated by the method create_backup(), and
    load the instances in the AMS. Additionally, in deletes the pickle files.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param path: string indicating the path to store the back-up files.
    """
    if type(path) is not str:
        raise ValueError("path parameter has to be a String.")

    # read pickle files stored information
    with open(os.path.join(path, 'nodes_backup.pickle'), 'rb') as handle:
        nodes = pickle.load(handle)
    with open(os.path.join(path, 'relations_backup.pickle'), 'rb') as handle:
        relations = pickle.load(handle)
    with open(os.path.join(path, 'non_instantiable_backup.pickle'), 'rb') as handle:
        non_insta = pickle.load(handle)
    # get the new id from each of the non instantiable classes and store to a dictionary
    id_dict = {}
    for nn in non_insta:
        new_id = get_node_by_name(driver, nn['name'], nn['class'], to_dict=True)
        if new_id is not None:
            id_dict[int(nn['old_id'])] = int(new_id['identifier'])
    # firstly, load nodes into the database
    print("Inserting back-up nodes ...")
    for node2 in nodes:
        # extract the node metadata
        old_id = node2.pop('identifier')
        cls = node2.pop('class')
        # extract the node attributes
        info_node = {str(key): value for key, value in node2.items()}
        # insert the node in the db
        new_node = insert_instance(driver=driver, class_type=cls, param_dict=info_node)
        # relate the old_id with the new one
        id_dict[int(old_id)] = int(new_node['swai__identifier'])
    # insert all the relations that exists in the previous version of the database
    print("nodes inserted, start the relation back-up insertions ...")
    for relation in relations:
        if relation['relation'] in ['score', 'scored in']:
            modify_score(driver=driver, start_node_id=id_dict[relation['start_node']],
                         end_node_id=id_dict[relation['end_node']], score=relation['score'])
        else:
            modify_instance(driver=driver, neo_id=id_dict[relation['start_node']], add_dict={
                relation['relation']: get_node_by_id(driver, id_dict[relation['end_node']])['rdfs__label']})

    print("Back-up done! :)")
    # delete the pickle files after read them
    os.remove(os.path.join(path, 'nodes_backup.pickle'))
    os.remove(os.path.join(path, 'relations_backup.pickle'))
    os.remove(os.path.join(path, 'non_instantiable_backup.pickle'))


def create_instance_of(driver: NeoDriver):
    """
    This function create the instanceOf relations that the current Neo4j importing function is not capable to do.
    :param driver: Driver object which contains the connection to the database and the static variables
    """
    classes = list(driver.class_owl["label"])
    for cls in classes:
        cls, prx = get_dbclass_prefix(driver=driver, class_label=cls)
        if prx is None:
            continue
        # get the correct Class node for the relation
        end_nodes = driver.graph.nodes.match("owl__Class", swai__ontologyName=cls).all()
        end_node = None
        # quit external nodes (Equivalent classes)
        if len(end_nodes) == 1:
            end_node = end_nodes[0]
        else:
            for end in end_nodes:
                if driver.prefixes['swai'] in end['uri']:
                    end_node = end
        for start_node in driver.graph.nodes.match(prx+'__'+cls).all():
            rel = Relationship(start_node, "owl__instanceOf", end_node)
            # if it doesn't exist, add the relationship to the DB
            if rel not in driver.graph.match((start_node,), r_type="owl__instanceOf").all():
                driver.graph.create(rel)
