from .NeoDriver import NeoDriver
from .getters import *
from py2neo import Relationship
from .node import *


def insert_relation(driver: NeoDriver, start_node_id: int, end_node_id: int, relation: str, value=None):
    """
    Insert a node relation from the AMS.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param start_node_id: Identifier of the instance (start node) from which you want to insert a relation.
    :param end_node_id: Identifier of the instance (end node) from which you want to insert a relation.
    :param relation: relation type. Must be a relation defined in the ontology.
    :param value: value to insert in the specified relation
    """
    # ERROR CONTROL #
    # if the starting node from which the relation wants to be inserted does not exist
    if not if_node_exist_by_id(driver, start_node_id) or 'owl__NamedIndividual' not in str(get_node_by_id(driver, start_node_id).labels):
        raise ValueError(
            'Relation insertion is cancelled, the following instance ID does not belongs to any instance in the Database: ' + str(start_node_id))
    # if the ending node to which the relation wants to be inserted does not exist
    if not if_node_exist_by_id(driver, end_node_id) or 'owl__NamedIndividual' not in str(get_node_by_id(driver, end_node_id).labels):
        raise ValueError(
            'Relation insertion is cancelled, the following instance ID does not belongs to any instance in the Database: ' + str(
                end_node_id))

    # check if the relation type exist
    if relation not in driver.properties["label"].values:
        raise ValueError("The relation type: " + relation + " it is not a relation type in the AMS")

    # get the nodes, and their classes
    start_node = get_node_by_id(driver, start_node_id)
    end_node = get_node_by_id(driver, end_node_id)
    start_node_class = driver.graph.match([start_node], r_type='owl__instanceOf').first().end_node['rdfs__label']
    end_node_class = driver.graph.match([end_node], r_type='owl__instanceOf').first().end_node['rdfs__label']

    # if the relation is not in the starting node class domain
    df_row = driver.properties.loc[driver.properties["label"] == relation]
    if start_node_class not in df_row["domain"].to_list()[0]:
        raise ValueError(start_node_class + " class it's not in the domain of the relation: " + relation)
    # if the relation is not in the ending node class range
    if end_node_class not in df_row["range"].to_list()[0]:
        raise ValueError(end_node_class + " class it's not in the range of the relation: " + relation)

    # if relation already exists
    if if_relation_exists(driver, start_node_id, end_node_id, relation):
        raise ValueError(
            "The relation: " + relation + " between the nodes with the IDs: " + str(start_node_id) + " and " + str(
                end_node_id) + " can't be inserted because it already exists.")

    # if relation is score or scored in
    if relation in ['score', 'scored in']:
        if value is None:
            raise ValueError(
                "The relation {0} from {1} to {2} requires a valid score value.".format(relation, start_node_id, end_node_id))
        rel = Relationship(start_node, "swai__" + df_row["db_name"].values[0], end_node, score=float(value))
        inv_rel = Relationship(end_node, "swai__" + driver.inverses[df_row["db_name"].values[0]], start_node, score=float(value))
        driver.graph.create(inv_rel)
    else:
        rel = Relationship(start_node, "swai__" + df_row["db_name"].values[0], end_node)
    driver.graph.create(rel)

    # if the relation has inverse relation associated
    if driver.inverses[df_row["db_name"].values[0]]:
        if relation not in ['score', 'scored in']:
            inv_rel = Relationship(end_node, "swai__" + driver.inverses[df_row["db_name"].values[0]], start_node)
            # add the relationship to the DB
            driver.graph.create(inv_rel)


def delete_relation(driver: NeoDriver, start_node_id: int, end_node_id: int, relation: str):
    """
    Delete a node relation from the AMS.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param start_node_id: Identifier of the start instance (node) from which you want to delete the relation.
    :param end_node_id: Identifier of the start instance (node) from which you want to delete the relation.
    :param relation: relation type. Must be a relation defined in the ontology.
    """
    # ERROR CONTROL #
    # if the start node from which the relation wants to be removed does not exist
    if not if_node_exist_by_id(driver, start_node_id):
        raise ValueError(
            'Relation deletion cancelled, the following instance ID does not belongs to any instance in the Database: ' + str(
                start_node_id))
    # if the end node from which the relation wants to be removed does not exist
    if not if_node_exist_by_id(driver, end_node_id):
        raise ValueError(
            'Relation deletion cancelled, the following instance ID does not belongs to any instance in the Database: ' + str(
                end_node_id))

    # if the relation type exist
    if relation not in driver.properties["label"].values:
        raise ValueError("The relation type: " + relation + " does not exist in the AMS.")

    # if the relation does not exist
    if not if_relation_exists(driver, start_node_id, end_node_id, relation):
        raise ValueError("The relation: " + relation + " between the nodes with the IDs: "+str(start_node_id)+" and "+str(end_node_id)+" can't be deleted because does not exist.")

    # RELATION DELETION #
    # delete the relationship in the AMS
    df_row = driver.properties.loc[driver.properties["label"] == relation]
    driver.graph.run(
        'MATCH(a:owl__NamedIndividual)-[r:{0}]->(b:owl__NamedIndividual) WHERE ID(a) = {1} AND ID(b) = {2} DELETE r'.format(
            'swai__' + df_row["db_name"].values[0], str(start_node_id), str(end_node_id)),)

    # delete inverse relation, if exists
    if driver.inverses[df_row["db_name"].values[0]]:
        inv_rel_db = driver.inverses[df_row["db_name"].values[0]]
        # delete the relationship in the AMS
        driver.graph.run(
            'MATCH(a:owl__NamedIndividual)-[r:{0}]->(b:owl__NamedIndividual) WHERE ID(a) = {1} AND ID(b) = {2} DELETE r'.format(
                'swai__' + inv_rel_db, str(end_node_id), str(start_node_id)),)


def if_relation_exists(driver: NeoDriver, start_node_id: int, end_node_id: int, relation: str):
    if relation not in driver.properties["label"].values:
        raise ValueError("The relation type: " + relation + " does not exist in the AMS.")
    # Search the db name
    rel_row = driver.properties.loc[driver.properties["label"] == relation]
    db_relation = rel_row["db_name"].values[0]
    # run the query
    query = """MATCH (a)-[r:{0}]->(b) WHERE id(a) = {1} AND id(b) = {2} RETURN COUNT(r) > 0 AS relationship_exists""".format("swai__" + str(db_relation), str(start_node_id), str(end_node_id))
    result = driver.graph.run(query, ).evaluate()
    return result
