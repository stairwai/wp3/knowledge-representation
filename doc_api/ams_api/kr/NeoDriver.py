from py2neo import Graph
import os
import urllib.request
import json
import pandas as pd

PATH_JSON = "https://gitlab.com/stairwai/wp3/knowledge-representation/-/raw/developer/swai_ams.json"


class NeoDriver:
    def __init__(self, is_reset: bool = False):
        # load the credentials of the database (normal port 7687)
        bolt_url = self.get_env_var('NEO4J_URL', 'bolt://localhost:7687')
        user = self.get_env_var('NEO4J_USER', 'neo4j')
        password = self.get_env_var('NEO4J_PASSWORD', 's3cr3t')

        if is_reset:
            # load all the static data-structures from the file
            with urllib.request.urlopen(PATH_JSON) as url:
                res = json.load(url)
            file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "swai_ams.json")
            # Save to a file in the script's/module's directory
            with open(file_path, "w") as file:
                json.dump(res, file)

        # initialize the connection to the Neo4j database
        self.graph = Graph(bolt_url, auth=(user, password))

        # Import fix data-structures from the static file
        (self.uri, self.class_owl, self.properties, self.inverses, self.technique_hierarchy,
         self.business_hierarchy, self.insta_classes, self.forbidden_clauses, self.hwp2env, self.owg_terms,
         self.prefixes) = self.read_var_file()

    @staticmethod
    def get_env_var(name, default=None):
        if name in os.environ:
            return os.environ[name]
        else:
            return default

    @staticmethod
    def read_var_file():
        """
        This method load all the fix data-structures of the AMS from a json file.
        """
        # load all the static data-structures from the file
        file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "swai_ams.json")
        if os.path.exists(file_path):
            with open(file_path, "r") as file:
                res = json.load(file)
        else:
            # load all the static data-structures from the file
            with urllib.request.urlopen(PATH_JSON) as url:
                res = json.load(url)
            # Save to a file in the script's/module's directory
            with open(file_path, "w") as file:
                json.dump(res, file)

        return (res['uri'], pd.DataFrame.from_dict(res['class_owl'], orient='index'),
                pd.DataFrame.from_dict(res['properties'], orient='index'), res['inverses'], res['technique_hierarchy'],
                res['business_hierarchy'], res['insta_classes'], res['forbidden_clauses'], res['hwp2env'],
                res['owg_terms'], res['prefixes'])
