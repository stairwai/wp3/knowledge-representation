# Modification Log File

All the AMS-API suggested changes are stored in this file. You will be able to find a table with the suggested change, the date of the suggestion, and the source of the suggestion.

## AMS-API Suggested Changes

Date | Description                                                                                                                                                   | Source
:--- |:--------------------------------------------------------------------------------------------------------------------------------------------------------------| :---
12-2022 | Implementation of unique IDs for each node, inserted in the AMS based on Neo4j identifiers, which are not consistent over reset but are unique.               | WP5
12-2022 | Node search engine based on name-class tuple changed to one based on IDs (identity extracted from Neo4j).                                                     | WP3 (internal)
11-2022 | AMS backup functionalities implemented.                                                                                                                       | WP5
12-2022 | Time optimization for modifying/inserting instance methods.                                                                                                   | WP5
01-2023 | Add AI Expert specific support functionalities to StairwAI AMS.                                                                                               | WP5
02-2023 | Modify AMS methods which extract AI Techniques and Business categories to easily retrieve the term descriptions.                                              | WP5
02-2023 | Insert unique UUID identifiers during the instance insertions.                                                                                                | WP5
03-2023 | Node2data output fields labels adapted and standardized in agreement with Michele.                                                                            | WP5
04-2023 | Addition of automatic backup function, the suggestions include the requirement to set a specific hour for the backup and require the path to store the files. | WP5-WP3
06-2023 | modify_instance function modified to concurrently accept property deletions and property additions.                                                           | WP5
08-2023 | AMS-API adaptation to be able to handle WP6 benchmark output files.                                                                                           | WP6-WP3
10-2023 | Add Educational Resource specific support functionalities to StairwAI AMS.                                                                                    | WP5
12-2023 | AMS-API new version with the addition of updated documentation and automatic test files.                                                                      | WP3
12-2023 | Add Academic Resource specific support functionalities to StairwAI AMS.                                                                                       | WP5
01-2024 | AMS-API insertion time optimizations and minor changes on the accepted data format of the scores.                                                             | WP5-WP3
01-2024 | AMS-API functionalities tests cases addition and modification of the AMS-API code structure to be more inteligible.                                           | WP3
01-2024 | Addition of the Documentation PDF for the AMS-API and ontology correct maintanment guidelines.                                                                | WP3

