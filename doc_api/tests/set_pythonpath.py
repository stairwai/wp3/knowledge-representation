import os
import sys
from pathlib import Path


def find_package_path(package_name):
    for path in sys.path:
        full_path = Path(path) / package_name
        if full_path.exists():
            return full_path
    return None


package_path = find_package_path('ams_api')

if package_path:
    os.environ['PYTHONPATH'] = str(package_path) + os.pathsep + os.environ.get('PYTHONPATH', '')
    print(f"PYTHONPATH set to include {package_path}")
else:
    print("ams_api package not found.")
