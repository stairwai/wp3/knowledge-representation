#!/bin/bash 

# install the requirements
pip install -r requirements.txt

# start database docker
sudo docker-compose up -d

# Loop until the docker is started condition is met
until sudo docker logs json_generator_neo4j-service_1 | grep "Started."; do
  sleep 2
done

# run the json file generator to update the JSON
python ./ams_api/setup.py

# Remove docker compose
sudo docker stop json_generator_neo4j-service_1
sudo docker rm json_generator_neo4j-service_1
