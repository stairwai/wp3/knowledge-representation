import pandas as pd
import json
from py2neo import Graph
from py2neo.data import Relationship

# NON_INSTANTIABLE_CLASS = ['Agent', 'AI Asset', 'AI Technique', 'AI Artifact', 'Country', 'Language', 'Container','Organisation Type', 'Education level', 'Education type', 'Skill', 'Pace', 'Distribution','Criticality', 'AI Capability', 'Significant', 'Limited', 'Little or no damage', 'Sense','Unacceptable', 'Process & understand', 'Communication', 'Act', 'Very significant','Hardware Platform', 'Environment']

# EXTRA_CLASSES = ['Philosophy of AI', 'Cross cutting concepts', 'Organization', 'organización', 'Philosophy of AI', 'AI Ethics']

INSTANTIABLE_CLASS = ['Library', 'AI Hardware Component', 'Course', 'Open Call', 'Need', 'Problem Statement',
                      'Algorithm', 'Job Posting', 'Educational Resource', 'Measurement', 'Benchmark', 'Model',
                      'Person', 'Post', 'Tool', 'Academic Resource', 'AI expert', 'Certification', 'Dataset',
                      'Solution', 'Metric', 'Organisation']

FORBIDDEN_CLAUSES = ['CREATE', 'DELETE', 'SET', 'REMOVE', 'MERGE', 'FOREACH', 'LOAD CSV']

HWP2ENV = {"imx8m-nano": "IntelPokyLinux", "intel-core-i5": "MacOSx", "stm32mp1": "OpenSTlinux", "jetson-nano": "Ubuntu18",
           "jetson-xav-agx": "Ubuntu18", "rpi3": "Ubuntu20", "rpi4": "Ubuntu20", "x86-64-nuc": "Ubuntu20"}

ONTO_PREFIX = ["swai", "owg", "aitec", "cry", "aicat"]


class Driver:
    def __init__(self, uri_l, user_n, password_n):
        # initialize the connection to the Neo4j database
        self.graph = Graph(uri_l, auth=(user_n, password_n))
        # Initialize the database
        self.init_db()
        # Generate fix data-structures from the static file
        self.var_file_init()

    def var_file_init(self):
        """
        This method creates from scratch the model data-structures of the AMS using the StairwAI Ontology structure
        imported. Finally, store it in a JSON file.
        """
        uri = str.split(self.graph.nodes.match("owl__Class", rdfs__label="AI Asset").first()["uri"], sep='#')[0] + "#"
        # get all the ontology classes avoiding repetitions and None values (avoiding classes that have not ontologyNam)
        class_owl = list(set([(dtype["rdfs__label"], dtype["swai__ontologyName"]) for dtype in
                              self.graph.nodes.match("owl__Class").all()]))
        class_owl = [i for i in class_owl if i[0] and i[0] not in ['Organization', 'organización'] and i[1]]
        class_owl = pd.DataFrame(class_owl, columns=["label", "db_name"])
        # get classes from AI Techniques hierarchy
        technique_infer_class = self.inference_subclasses(['AI Technique'], class_owl)
        # get classes from Business categories
        bc_infer_class = self.inference_subclasses(['Business categories'], class_owl)
        # extract the properties with all its elements
        properties = self.__extract_properties(class_owl)
        # inverse of list
        inverses = self.get_inverse_dict()
        # Get the Ontology working group terms, if they exists
        owg_terms = [elem for elem in class_owl["label"].to_list() if
                     str.split(self.graph.nodes.match("owl__Class", rdfs__label=elem).first()["uri"], sep='#')[
                         0] == "http://purl.org/owg/onto/spec"]
        # extract the prefixes table as a dict, avoiding the ontology system prefixes
        prefixes = {record["prefix"]: record['namespace'] for record in self.graph.run("CALL n10s.nsprefixes.list")
                    if record["prefix"] in ONTO_PREFIX}
        # create the necessary instanceOf relations
        self.create_instance_of(class_owl, technique_infer_class, bc_infer_class, owg_terms)
        # if the static file that contains all the fix database data has to be modified, modify it
        dictionary = {'uri': uri, 'class_owl': class_owl.to_dict('index'),
                      'properties': properties.to_dict('index'), "insta_classes": INSTANTIABLE_CLASS,
                      'inverses': inverses, 'technique_hierarchy': technique_infer_class,
                      'business_hierarchy': bc_infer_class, 'forbidden_clauses': FORBIDDEN_CLAUSES,
                      'hwp2env': HWP2ENV, "owg_terms": owg_terms, "prefixes": prefixes}
        with open('../swai_ams.json', "w") as outfile:
            json.dump(dictionary, outfile)
        print("json file generated/updated correctly")

    def get_inverse_dict(self):
        """
        This method is used to generate the dictionary where the inverse relations are stored, obtaining a mapping
        between inverse relations extracted from the ontology imported.
        :return: a dictionary in which the inverse relation is obtained based on key-value relation.
        """
        # get all the object properties of the ontology
        res_dict = {}
        for node in self.graph.nodes.match("owl__ObjectProperty").all():
            # find the database name of the property
            db_name = node["swai__ontologyName"]
            # find the db_name of the inverse relation
            inverse_rel = self.graph.match((node,), r_type="owl__inverseOf").first()
            inverse_db_name = inverse_rel.end_node["swai__ontologyName"] if inverse_rel is not None else None
            # if it does not have an inverse, set the value to none
            if inverse_db_name is None and db_name not in list(res_dict.keys()):
                res_dict[db_name] = None
            # if the relation has an inverse
            if inverse_db_name is not None:
                res_dict[db_name] = inverse_db_name
                res_dict[inverse_db_name] = db_name
        return res_dict

    def __extract_properties(self, class_owl):
        """
        This function extract the main characteristics of all the current properties (data or object properties), and
        store them in a pandas dataframe. Characteristics such as: label, DB name, domain, range, etc.
        :return: it returns the generated dataframe.
        """
        # get all the data properties of the ontology
        data = []
        for node in self.graph.nodes.match("owl__DatatypeProperty").all():
            # find the label and the database name of the property
            label = node["rdfs__label"]
            db_name = node["swai__ontologyName"]
            # find the labels of all the domain classes
            domain = []
            domain_rel = self.graph.match((node,), r_type="rdfs__domain").first()
            domain_node = domain_rel.end_node if domain_rel is not None else None
            if domain_node is not None and domain_node["rdfs__label"] is None and domain_node in self.graph.nodes.match(
                    "owl__Class", ).all():
                domain += self.__recursive_search(
                    self.graph.match((domain_node,), r_type="owl__unionOf").first().end_node, [])
            elif domain_node is not None and domain_node["rdfs__label"] is not None:
                domain.append(domain_node["rdfs__label"])
            # inference the subdomain
            domain = self.inference_subclasses(domain, class_owl)
            domain = [d for d in domain if d not in ['Organization', 'organización']]
            data.append(
                {'label': label, 'db_name': db_name, 'domain': domain, 'range': None, 'type': 'DatatypeProperty'})

        # get all the object properties of the ontology
        for node in self.graph.nodes.match("owl__ObjectProperty").all():
            # find the label and the database name of the property
            label = node["rdfs__label"]
            db_name = node["swai__ontologyName"]
            # find the labels of all the domain classes
            domain = []
            domain_rel = self.graph.match((node,), r_type="rdfs__domain").first()
            domain_node = domain_rel.end_node if domain_rel is not None else None
            if domain_node is not None and domain_node["rdfs__label"] is None and domain_node in self.graph.nodes.match(
                    "owl__Class", ).all():
                domain += self.__recursive_search(
                    self.graph.match((domain_node,), r_type="owl__unionOf").first().end_node, [])
            elif domain_node is not None and domain_node["rdfs__label"] is not None:
                domain.append(domain_node["rdfs__label"])
            # inference the subdomain
            domain = self.inference_subclasses(domain, class_owl)
            domain = [d for d in domain if d not in ['Organization', 'organización']]
            # find the labels of all the range classes
            neo_range = []
            range_rel = self.graph.match((node,), r_type="rdfs__range").first()
            range_node = range_rel.end_node if range_rel is not None else None
            if range_node is not None and range_node["rdfs__label"] is None and range_node in self.graph.nodes.match(
                    "owl__Class", ).all():
                neo_range += self.__recursive_search(
                    self.graph.match((range_node,), r_type="owl__unionOf").first().end_node, [])
            elif range_node is not None and range_node["rdfs__label"] is not None:
                neo_range.append(range_node["rdfs__label"])
            # inference the sub-range
            neo_range = self.inference_subclasses(neo_range, class_owl)
            neo_range = [r for r in neo_range if r not in ['Organization', 'organización']]
            # store the data of the node in the list of dictionaries
            data.append(
                {'label': label, 'db_name': db_name, 'domain': domain, 'range': neo_range, 'type': 'ObjectProperty'})
        return pd.DataFrame(data, columns=["label", "db_name", "domain", "range", "type"])

    def inference_subclasses(self, classes, class_owl):
        """
        This method is used to do a recursive search through the ontology hierarchy with the aim of finding the children
        classes of each class in a set of classes.
        :param classes: 1D array of classes in the ontology hierarchy.
        :param class_owl: data structure generated during the JSON file creation.
        :return: a set of classes composed by the original classes and the new ones.
        """
        if len(classes) > 0:
            for class_name in classes:
                name = self.graph.run(
                    "MATCH (n:owl__Class {rdfs__label: $topic_name})<-[r:rdfs__subClassOf*]-(child:owl__Class)"
                    "RETURN child.rdfs__label", topic_name=str(class_name))
                if name is not None:
                    classes += list(name.to_ndarray().ravel())
            classes = list(set(classes))
        else:
            classes = class_owl.copy()['label'].tolist()
        return classes

    def __recursive_search(self, inter_node, labels):
        """
        This function is the recursive part of the characteristics' extraction (range and domain) from all the properties
        :param inter_node: node from which we will analise the connections.
        :param labels: the extracted labels of the classes found.
        :return: the labels
        """
        # find the desired connected nodes
        rel_eval = self.graph.match((inter_node,), r_type="rdf__first").first()
        # if empty, return empty list
        if rel_eval is None:
            return []
        elif rel_eval.end_node["rdfs__label"] is not None:
            labels += [rel_eval.end_node["rdfs__label"]]
        # recursive call searching for more categories
        labels += self.__recursive_search(self.graph.match((inter_node,), r_type="rdf__rest").first().end_node, labels)
        return list(set(labels))

    def create_instance_of(self, class_owl, ai_techniques, business, owg_terms):
        """
        This function create the instanceOf relations that the current Neo4j importing function is not capable to do.
        :param class_owl: data structure generated during the JSON file creation.
        :param ai_techniques: lis of the AI Techniques in the AMS
        :param business: business areas extracted from the AMS
        :param owg_terms: ontology working group terms.
        """
        classes = list(class_owl["label"])
        for cls in classes:
            # choose the prefix required
            if cls == "Country":
                prx = 'cry__'
            elif cls in business:
                prx = 'aicat__'
            elif cls in ai_techniques:
                prx = 'aitec__'
            elif cls in owg_terms:
                prx = 'owg__'
            else:
                prx = "swai__"
            # from label to db_name
            cls = class_owl.loc[class_owl["label"] == cls].values[0][1]
            # get the correct Class node for the relation
            end_nodes = self.graph.nodes.match("owl__Class", swai__ontologyName=cls).all()
            end_node = None
            # quit external nodes (Equivalent classes)
            if len(end_nodes) == 1:
                end_node = end_nodes[0]
            else:
                for end in end_nodes:
                    if "swai/spec" in end["uri"] or 'stairwai' in end["uri"]:
                        end_node = end
            # generate the relations
            for start_node in self.graph.nodes.match(prx + cls).all():
                rel = Relationship(start_node, "owl__instanceOf", end_node)
                # if it doesn't exist, add the relationship to the DB
                if rel not in self.graph.match((start_node,), r_type="owl__instanceOf").all():
                    self.graph.create(rel)

    def init_db(self):
        """
        This function initialize a StairwAI AMS new instance running the importing ontology commands and setting the
        prefixes required.
        """
        # initialise the database using the commands to set the prefixes and loading the ontologies
        print("Importing ontologies...")
        self.graph.run("CALL n10s.graphconfig.init();")
        self.graph.run("CREATE CONSTRAINT n10s_unique_uri ON (r:Resource) ASSERT r.uri IS UNIQUE;")
        self.graph.run("CALL n10s.nsprefixes.add('cry', 'https://stairwai.gitlab.io/wp3/ontology/Country.rdf#');")
        self.graph.run("CALL n10s.nsprefixes.add('aicat', 'https://stairwai.gitlab.io/wp3/ontology/topics.rdf#');")
        self.graph.run("CALL n10s.nsprefixes.add('aitec', 'https://stairwai.gitlab.io/wp3/ontology/ai_onto.rdf#');")
        self.graph.run("CALL n10s.nsprefixes.add('swai', 'http://purl.org/swai/spec#');")
        self.graph.run("CALL n10s.nsprefixes.add('owg', 'http://purl.org/owg/onto/spec#');")
        self.graph.run("CALL n10s.nsprefixes.add('foaf', 'http://xmlns.com/foaf/0.1/');")
        self.graph.run("CALL n10s.nsprefixes.add('dc', 'http://purl.org/dc/elements/1.1/');")
        self.graph.run("CALL n10s.nsprefixes.add('dcterms', 'http://purl.org/dc/terms/');")
        self.graph.run("CALL n10s.rdf.import.fetch('http://purl.org/swai/spec','RDF/XML');")
        self.graph.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/topics.rdf', 'RDF/XML');")
        self.graph.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/Country.rdf', 'RDF/XML');")
        self.graph.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/ai_onto.rdf', 'RDF/XML');")


# Generate the json file
driver = Driver(uri_l='bolt://localhost:7687', user_n='neo4j', password_n='s3cr3t')
